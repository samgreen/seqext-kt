package io.samg.seqext

import org.amshove.kluent.`should equal`
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class `Scan tests` {
    @Nested
    inner class `Given a Sequence of Ints` {
        private lateinit var sequence: Sequence<Int>

        @BeforeEach
        fun setup() {
            sequence = (1..5).asSequence()
        }

        @Nested
        inner class `on scanning without a seed` {
            @Test
            fun `should provide accumulated value`() {
                sequence.scan { acc, it ->
                    acc `should equal` it - 1
                    it
                }
            }

            @Test
            fun `should transform`() {
                sequence.scan { _, it -> it + 1 }.toList() `should equal` listOf(3, 4, 5, 6)
            }
        }

        @Nested
        inner class `on scanning with a seed` {
            @Test
            fun `should provide accumulated value`() {
                sequence.scan(0) { acc, it ->
                    acc `should equal` it - 1
                    it
                }
            }

            @Test
            fun `should transform`() {
                sequence.scan(0) { _, it -> it + 1 }.toList() `should equal` listOf(2, 3, 4, 5, 6)
            }
        }

        @Nested
        inner class `on boxed scanning` {
            @Test
            fun `should provide mutable state`() {
                sequence.scanBoxed(0) { acc, it ->
                    acc.state `should equal` it - 1
                    acc.state = it
                }
            }

            @Test
            fun `should transform`() {
                sequence.scanBoxed(0) { _, it -> it + 1 }.toList() `should equal` listOf(2, 3, 4, 5, 6)
            }
        }
    }
}
