package io.samg.seqext

/**
 * A mutable container for a single variable `state`.
 */
data class Box<T>(var state: T)
