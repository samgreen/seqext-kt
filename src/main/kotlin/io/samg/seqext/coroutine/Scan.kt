package io.samg.seqext.coroutine

import io.samg.seqext.Box
import kotlin.coroutines.experimental.buildSequence

/**
 * Somewhere between `map` and `fold`. Returns a sequence transformed by the given `operation`, which is provided with
 * the return value of the previous iteration, or the first item of the sequence on the first iteration.
 *
 * Because it consumes the first item to provide the accumulator for the first iteration, the returned sequence
 * is one element shorter. (This may change in the future?)
 */
fun <T : R, R> Sequence<T>.scan(operation: (acc: R, it: T) -> R): Sequence<R> = buildSequence {
    val iter = this@scan.iterator()
    var acc: R = if (!iter.hasNext()) {
        throw UnsupportedOperationException("Empty sequence can't be scanned without an initial value.")
    } else {
        iter.next()
    }
    for (it in iter) {
        acc = operation(acc, it)
        yield(acc)
    }
}

/**
 * Somewhere between `map` and `fold`. Returns a sequence transformed by the given `operation`, which is provided with
 * the return value of the previous iteration.
 */
fun <T, R> Sequence<T>.scan(initial: R, operation: (acc: R, it: T) -> R): Sequence<R> = buildSequence {
    var acc = initial
    for (it in this@scan) {
        acc = operation(acc, it)
        yield(acc)
    }
}

/**
 * Somewhere between `map` and `fold`. Returns a sequence transformed by the given `operation` which is provided with a
 * `Box<S>`, a simple container with a mutable property `state` seeded with the `initial` value.
 */
fun <T, S, R> Sequence<T>.scanBoxed(initial: S, operation: (acc: Box<S>, it: T) -> R): Sequence<R> = buildSequence {
    val acc = Box(initial)
    for (it in this@scanBoxed) {
        yield(operation(acc, it))
    }
}
