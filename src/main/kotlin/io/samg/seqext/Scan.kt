package io.samg.seqext

/**
 * Somewhere between `map` and `fold`. Returns a sequence transformed by the given `operation`, which is provided with
 * the return value of the previous iteration, or the first item of the sequence on the first iteration.
 *
 * Because it consumes the first item to provide the accumulator for the first iteration, the returned sequence
 * is one element shorter. (This may change in the future?)
 */
fun <T : R, R> Sequence<T>.scan(operation: (acc: R, it: T) -> R): Sequence<R> = object : Sequence<R> {
    override fun iterator(): Iterator<R> = object : Iterator<R> {
        val iter = this@scan.iterator()
        var acc: R = if (!iter.hasNext()) {
            throw UnsupportedOperationException("Empty sequence can't be scanned without an initial value.")
        } else {
            iter.next()
        }

        override fun next(): R {
            acc = operation(acc, iter.next())
            return acc
        }

        override fun hasNext(): Boolean = iter.hasNext()
    }
}

/**
 * Somewhere between `map` and `fold`. Returns a sequence transformed by the given `operation`, which is provided with
 * the return value of the previous iteration, or `initial` on the first iteration.
 */
fun <T, R> Sequence<T>.scan(
        initial: R,
        operation: (acc: R, it: T) -> R
): Sequence<R> = object : Sequence<R> {
    override fun iterator(): Iterator<R> = object : Iterator<R> {
        val iter = this@scan.iterator()
        var acc: R = initial

        override fun next(): R {
            acc = operation(acc, iter.next())
            return acc
        }

        override fun hasNext(): Boolean = iter.hasNext()
    }
}

/**
 * Somewhere between `map` and `fold`. Returns a sequence transformed by the given `operation` which is provided with a
 * `Box<S>`, a simple container with a mutable property `state` seeded with the `initial` value.
 */
fun <T, S, R> Sequence<T>.scanBoxed(
        initial: S,
        operation: (acc: Box<S>, it: T) -> R
): Sequence<R> = object : Sequence<R> {
    override fun iterator(): Iterator<R> = object : Iterator<R> {
        val iter = this@scanBoxed.iterator()
        val acc: Box<S> = Box(initial)

        override fun next(): R = operation(acc, iter.next())

        override fun hasNext(): Boolean = iter.hasNext()
    }
}

