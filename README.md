[![pipeline status](https://gitlab.com/samgreen/seqext-kt/badges/master/pipeline.svg)](https://gitlab.com/samgreen/seqext-kt/commits/master)
[![Download](https://api.bintray.com/packages/samgreen/maven/seqext/images/download.svg)](https://bintray.com/samgreen/maven/seqext/_latestVersion)

# seqext

Provides Sequence extensions for Kotlin.

## Usage

Available from JCenter.

### Gradle

```
compile "io.samg:seqext:0.1.0"
```

## Extensions

### scan

Similar to `map` and `fold`. Transforms a sequence, passing along the previous value.

```kotlin
@Test
fun `partial sums`() {
    val nums = (1..5).asSequence()
    val sums = nums.scan(0) { acc, num ->
        acc + num
    }
    sums.toList() `should equal` listOf(1, 3, 6, 10, 15)
}

```
### scanBoxed

Similar to `scan` but providing a `Box<S>`, a data object with one mutable field `state`, providing mutable state
that isn't necessarily related to the return value or type.

```kotlin
@Test
fun `products of partial sums`() {
    val nums = (1..5).asSequence()
    val products = nums.scanBoxed(0) { acc, num ->
        acc.state += num
        num * acc.state
    }
    products.toList() `should equal` listOf(1, 6, 18, 40, 75)
}
```
